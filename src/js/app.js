const products = [
	{
		title: "Diamond Earrings",
		img: "src/images/classy/pexels-the-glorious-studio-10983783.jpg",
		price: '16000',
		alt: "earrings",
		width: "400px"
	},

	{
		title: "Gold with Diamonds",
		img: "src/images/classy/pexels-the-glorious-studio-8891958.jpg",
		price: "61260",
		alt: "bracelet",
		width: "450px"
	},

	{
		title: "Gold Bracelet",
		img: "src/images/classy/pexels-the-glorious-studio-12026054.jpg",
		price: "36000",
		alt: "bracelet",
		width: "400px"
	},

	{
		title: "Shop The Look",
		img: "src/images/classy/pexels-cottonbro-studio-9421333.jpg",
		price: "Learn More",
		alt: "full look",
		width: "500px"
	},

	{
		title: "Leaf Diamonds",
		img: "src/images/classy/pexels-the-glorious-studio-7093761.jpg",
		price: "46000",
		alt: "bracelet",
		width: "380px"
	},

	{
		title: "Elsa Perreti",
		img: "src/images/classy/pexels-the-glorious-studio-10983785.jpg",
		price: "26000",
		alt: "necklace",
		width: "380px"
	}
]
let shoppingCard = [];

document.addEventListener("DOMContentLoaded", () => {
	const productsDiv = document.getElementById('products');
	products.forEach((p, idx) => {
		const productHTML = `
	<div class="col-md-6">
								<div id="1" class="p-3">
									<img src="${p.img}" width="${p.width}"
										alt="${p.alt}">
									<h5>${p.title}</h5>
									<div class="d-flex justify-content-between align-items-center"
										style="width: ${p.width}">
										<p>$ ${p.price}</p>
										<button id="btnAdd" onclick="productsToCard(${idx})">Buy</button>
									</div>
								</div>
						</div>`;
		productsDiv.innerHTML += productHTML;
	});
});


const productsToCard = (productIndex) => {
	let totalPrice = document.getElementById('total');
	totalPrice.innerHTML = parseInt(totalPrice.innerHTML) + parseInt(products[productIndex].price);

	// shoppingCart = [
	// 	...shoppingCart,
	// 	products[productIndex]
	// ]

	shoppingCard.push(products[productIndex]);

	const card = document.querySelector('.list-group');
	card.innerHTML = '';
	shoppingCard.forEach((cardProduct) => {
		card.innerHTML += `<li>
		<a href="#" class="d-flex align-items-center list-group-item rounded m-1 p-3">
			<img class="me-3 ms-2" src="${cardProduct.img}" alt="${cardProduct.alt}" width="150px">
			<div>
			<h5 class="titles">${cardProduct.title}</h5>
			<p class="mb-0 small pe-3">Price: $${cardProduct.price}</p>
			</div>
		</a>
	</li>`;
	})
}
